package ru.vlsu.isse.parprog.gol;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import ru.vlsu.isse.parprog.gol.model.Field;

public class Generate
{
    public static void main(String[] args) throws IOException
    {
        if (args.length != 4)
        {
            printUsage();
            System.exit(1);
        }
        
        Field field = new Field(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        
        Random random = new Random(Long.parseLong(args[2]));
        
        for (int x = 0; x < field.height(); x++)
        {
            for (int y = 0; y < field.width(); y++)
            {
                field.set(x, y, random.nextBoolean());
            }
        }
        
        field.saveToFile(new File(args[3]));
    }

    private static void printUsage()
    {
        System.out.println("usage: "
                + Generate.class.getName() + " <width> <height> <seed> <outputFile>");
    }
}
