package ru.vlsu.isse.parprog.gol.model.barriers;

public final class BlockingBarrier implements Barrier
{

    public synchronized void with(int index, Runnable runnable)
    {
        runnable.run();
    }

}
