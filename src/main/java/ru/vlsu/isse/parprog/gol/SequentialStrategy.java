package ru.vlsu.isse.parprog.gol;

import ru.vlsu.isse.parprog.gol.model.Field;

public class SequentialStrategy implements GameStrategy
{
    public void updateField(final Field source, final Field dest, final int offset, final int length)
    {
        for (int position = offset; position < offset + length; position++)
        {
            int x = position / source.width();
            int y = position - source.width() * x;
            
            int neighbours = (source.isAlive(x - 1, y - 1) ? 1 : 0)
                           + (source.isAlive(x - 1, y + 0) ? 1 : 0)
                           + (source.isAlive(x - 1, y + 1) ? 1 : 0)
                           
                           + (source.isAlive(x + 0, y - 1) ? 1 : 0)
                           + (source.isAlive(x + 0, y + 1) ? 1 : 0)
                           
                           + (source.isAlive(x + 1, y - 1) ? 1 : 0)
                           + (source.isAlive(x + 1, y + 0) ? 1 : 0)
                           + (source.isAlive(x + 1, y + 1) ? 1 : 0);
            
            if (neighbours == 3)
            {
                dest.set(x, y, true);
            }
            else if (neighbours < 2 || neighbours > 3)
            {
                dest.set(x, y, false);
            }
            else
            {
                dest.set(x, y, source.isAlive(x, y));
            }
        }
    }
}
