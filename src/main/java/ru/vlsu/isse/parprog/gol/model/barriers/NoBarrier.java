package ru.vlsu.isse.parprog.gol.model.barriers;

public final class NoBarrier implements Barrier
{

    public void with(int index, Runnable runnable)
    {
        runnable.run();
    }

}
