package ru.vlsu.isse.parprog.gol;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import ru.vlsu.isse.parprog.gol.model.Field;

public class ParallelStrategy implements GameStrategy
{
    private final long threshold;
    
    public ParallelStrategy(long threshold)
    {
        this.threshold = threshold;
    }
    
    public class GameOfLifeTask extends RecursiveAction
    {
        private static final long serialVersionUID = 4538797594874889336L;

        private final Field source;
        private final Field dest;
        private final int offset;
        private final int length;

        public GameOfLifeTask(Field source, Field dest, int offset, int length)
        {
            this.source = source;
            this.dest = dest;
            this.offset = offset;
            this.length = length;
            
            if (offset % 8 != 0)
            {
                throw new IllegalArgumentException("offset % 8 = " + (offset % 8) + ", must be 0");
            }
            
            if ((offset + length) % 8 != 0)
            {
                throw new IllegalArgumentException("(offset + length) % 8 = " + ((offset + length) % 8) + ", must be 0");
            }
        }

        @Override
        protected void compute()
        {
            //  There's 8 values in one cell, we don't want to break lower than that
            //  to avoid race conditions during field#set()
            int chunkSize = length / 2;
            if (chunkSize < 8 || length <= threshold)
            {
                new SequentialStrategy().updateField(source, dest, offset, length);
            }
            else
            {
                //  Round chunk size by 8
                int reminder = chunkSize % 8;
                RecursiveAction.invokeAll(
                        new GameOfLifeTask(source, dest, offset, chunkSize - reminder),
                        new GameOfLifeTask(source, dest, offset + chunkSize - reminder, chunkSize + reminder));
            }
        }
    }

    public void updateField(Field source, Field dest, int offset, int length)
    {
        new ForkJoinPool()
                .invoke(new GameOfLifeTask(source, dest, 0, length));
    }
}
