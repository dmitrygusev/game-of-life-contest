package ru.vlsu.isse.parprog.gol.model.barriers;

import java.util.concurrent.atomic.AtomicInteger;

public final class YieldBarrier implements Barrier
{
    private AtomicInteger lock = new AtomicInteger(-1);

    public void with(int index, Runnable runnable)
    {
        while (!lock.compareAndSet(-1, index))
        {
            Thread.yield();
        }
        runnable.run();
        lock.set(-1);
    }
}