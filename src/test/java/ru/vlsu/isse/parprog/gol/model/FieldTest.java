package ru.vlsu.isse.parprog.gol.model;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import ru.vlsu.isse.parprog.gol.model.Field;

public class FieldTest
{
    @Test
    public void testFieldAccessors()
    {
        Field field = new Field(4, 2);
        
        System.out.println(field);
        
        field.set(0, 1, true);
        field.set(0, 2, true);
        
        field.set(1, 0, true);
        field.set(1, 3, true);
        
        System.out.println(field);
        
        Assert.assertFalse(field.isAlive(0, 0));
        Assert.assertTrue(field.isAlive(0, 1));
        Assert.assertTrue(field.isAlive(0, 2));
        Assert.assertFalse(field.isAlive(0, 3));
        
        Assert.assertTrue(field.isAlive(1, 0));
        Assert.assertFalse(field.isAlive(1, 1));
        Assert.assertFalse(field.isAlive(1, 2));
        Assert.assertTrue(field.isAlive(1, 3));
        
        field.set(0, 0, true);
        field.set(0, 2, false);
        
        Assert.assertTrue(field.isAlive(0, 0));
        Assert.assertFalse(field.isAlive(0, 2));
    }
    
    @Test
    public void testSaveLoad() throws IOException
    {
        Field original = new Field(4, 4);
        
        for (int x = 0; x < 4; x++)
        {
            original.set(x, x, true);
        }
        
        System.out.println(original);
        
        File file = new File("build/field4x4.dat");
        
        if (file.exists())
        {
            Assert.assertTrue(file.delete());
        }
        
        original.saveToFile(file);
        
        Assert.assertTrue(file.exists());
        Assert.assertEquals(4 + 4 + original.size()/8, file.length());
        
        Field loaded = Field.loadFromFile(file);
        
        Assert.assertEquals(original, loaded);
    }
}
