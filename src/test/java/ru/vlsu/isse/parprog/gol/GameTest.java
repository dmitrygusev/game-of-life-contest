package ru.vlsu.isse.parprog.gol;

import static ru.vlsu.isse.parprog.gol.model.Field.loadFromFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ru.vlsu.isse.parprog.gol.model.ClosedField;

@RunWith(Parameterized.class)
public class GameTest
{
    @Parameters
    public static Collection<GameStrategy[]> data()
    {
        return Arrays.asList(new GameStrategy[][]
                {
                    { new SequentialStrategy() },
                    { new ParallelStrategy(8) }
                });
    }
    
    @Parameter
    public GameStrategy strategy;
    
    @Test
    public void testGameOctagon2Period5()
    {
        ClosedField field = new ClosedField(8, 8);
        
        field.init("   **   "
                 + "  *  *  "
                 + " *    * "
                 + "*      *"
                 + "*      *"
                 + " *    * "
                 + "  *  *  "
                 + "   **   ");
        
        Game game = new Game(field, strategy);
        
        System.out.println(game.getField());
        game.update(1);
        System.out.println(game.getField());
        game.update(1);
        System.out.println(game.getField());
        game.update(1);
        System.out.println(game.getField());
        
        ClosedField empty = new ClosedField(8, 8);
        
        Assert.assertEquals(empty, game.getField());
    }
    
    @Test
    public void testGameKokaGalaxyPeriod8()
    {
        ClosedField field = new ClosedField(16, 16);
        
        field.init("                "
                 + "                "
                 + "                "
                 + "   ** ******    "
                 + "   ** ******    "
                 + "   **           "
                 + "   **     **    "
                 + "   **     **    "
                 + "   **     **    "
                 + "          **    "
                 + "   ****** **    "
                 + "   ****** **    "
                 + "                "
                 + "                "
                 + "                "
                 + "                ");
        
        ClosedField copy = new ClosedField(field);
        
        Game game = new Game(field, strategy);
        
        System.out.println(game.getField());
        
        for (int i = 0; i < 8; i++)
        {
            game.update(1);
            System.out.println(game.getField());
        }
        
        Assert.assertEquals(copy, game.getField());
    }

    @Test
    public void testOctagon16x16() throws IOException
    {
        ClosedField field = new ClosedField(
                loadFromFile(new File("src/test/resources/octagon16x16.dat")));
        
        ClosedField copy = new ClosedField(field);
        
        Game game = new Game(field, strategy);
        
        System.out.println(game.getField());
        
        for (int i = 0; i < 5; i++)
        {
            game.update(1);
            System.out.println(game.getField());
        }
        
        Assert.assertEquals(copy, game.getField());
        
        game.update(5);
        System.out.println(game.getField());
        
        Assert.assertEquals(copy, game.getField());
    }
}
